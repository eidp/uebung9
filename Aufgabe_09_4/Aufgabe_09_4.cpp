/*** Aufgabe_09_4.cpp ***/
#include "Aufgabe_09_4.h"

using namespace std;

Fraction::Fraction() {
    numerator = 1;
    denominator = 1;
}

Fraction::Fraction(int const numerator, int const denominator) {
    this->numerator = numerator;
    this->denominator = denominator;

    normalize();
}

void Fraction::normalize() {
    if (denominator == 0) {
        numerator = 1;
        denominator = 1;
    } else if (numerator < 0 || denominator < 0) {
        if (denominator < 0) {
            denominator *= -1;
            numerator *= -1;
        }
    } else if (numerator < 0 && denominator < 0) {
        numerator *= -1;
        denominator *= -1;
    }

    int ggt = gcd(abs(numerator), denominator);

    numerator /= ggt;
    denominator /= ggt;
}

int Fraction::gcd(int const n, int const d) {
    if (d == 0)
        return n;
    else
        return gcd(d, n % d);
}

Fraction Fraction::operator+(Fraction const &f) {
    return Fraction((this->numerator * f.denominator) + (f.numerator * this->denominator), this->denominator * f.denominator);
}

Fraction Fraction::operator-(Fraction const &f) {
    return Fraction((this->numerator * f.denominator) - (f.numerator * this->denominator), this->denominator * f.denominator);
}

Fraction Fraction::operator*(Fraction const &f) {
    return Fraction(this->numerator * f.numerator, this->denominator * f.denominator);
}

Fraction Fraction::operator/(Fraction const &f) {
    return Fraction(this->numerator * f.denominator, this->denominator * f.numerator);
}

int Fraction::getNumerator() const {
    return numerator;
}

int Fraction::getDenominator() const {
    return denominator;
}

void Fraction::print() {
    std::cout << *this << endl;
}

ostream& operator<<(ostream &os, Fraction const &f) {
    os << f.getNumerator() << "/" << f.getDenominator();
    return os;
}

/*** Ende Aufgabe_09_4.cpp ***/
